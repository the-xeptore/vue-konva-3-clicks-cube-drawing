# Vue Konva 3 Clicks Cube Drawer

Vue Konva 3-Clicks Cube Drawer

See live at: <https://codesandbox.io/s/vue2-konva-3clicks-cube-drawing-lvipt>

## Project setup

```sh
pnpm install
```

### Compiles and hot-reloads for development

```sh
pnpm run serve
```

### Compiles and minifies for production

```sh
pnpm run build
```

### Lints and fixes files

```sh
pnpm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
